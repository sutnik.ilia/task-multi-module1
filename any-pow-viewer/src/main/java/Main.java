import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Main main = new Main();
        main.readFile(scannerInput());
    }

    public void readFile(String input) throws IOException {
        FileView fileViews = new FileView();
        fileViews.showFile(input);
    }

    public static String scannerInput() {
        Scanner inString = new Scanner(System.in);
        String s = inString.next();
        return s;
    }
}
