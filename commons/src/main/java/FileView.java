import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.util.List;

public class FileView {
        public void showFile(String fileName) throws IOException {
                File file = new File(fileName);
                List<String> lines = FileUtils.readLines(file, "UTF-8");

                for (String line : lines) {
                        System.out.println(line);
                }
        }
}
